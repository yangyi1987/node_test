import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
const crypto = require('crypto');
const https = require('https');

export class UserController {

    private userRepository = getRepository(User);

    private async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    private async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    private async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    private async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }

    private async login(request: Request, response: Response, next: NextFunction) {
        const token: string | undefined | null = request.get('token');
        !token ? create() : null;

        function create() {
            let url = 'https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code'
            https.get(url, callback).on('error', error);

            function callback(res) {
                let data = '';
                res.on('data', chunk=> {
                    data += chunk;
                })
            }

            function error(err) {
                response.sendStatus(201);
                response.send({code: -1, messes: "登录失败", data: null});
            }

        }
    }


}