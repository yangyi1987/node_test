const jwt = require('jsonwebtoken');

const secret = '123456789';


let token = jwt.sign({data: {name: '张三', age: 23, password: '123456'}}, secret, {expiresIn: '7 day'});
console.log("token", token);

jwt.verify(token, secret, callback)

function callback(err, decoded) {
    if(err) throw err;
    console.log(decoded);
}
