const jwt = require('jsonwebtoken');

const secret: string = '123456789';

// 创建 token
export function createToken(data: object): string {
    let token: string = jwt.sign({ data }, secret, {expiresIn: '7 day'})
    return token;
}

// 校验 token
export function verifyToken(token: string): any {
    let data = null;
    jwt.verify(token, secret, callback);
    function callback(err, decoded) {
        data = err ? false : decoded;
    }
    return data;
}

export function isArray(arr) {
    return arr && Array.isArray(arr) && arr.length > 0;
}
