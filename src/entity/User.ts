import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    nickname: string;

    @Column({})
    account: string;

    @Column({})
    password: string;

    @Column()
    mobile: string;

    @Column()
    openId: string;

    @Column({})
    avatarUrl: string;

    @Column()
    telnum: string;

    @Column({})
    country: string;

    @Column()
    gender: string;
}
